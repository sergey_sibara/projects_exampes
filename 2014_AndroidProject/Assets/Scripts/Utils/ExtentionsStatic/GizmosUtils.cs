﻿using EventSystem.Internal;
using UnityEngine;
using System.Collections;

public static class GizmosUtils
{
    public static void DrawSquareGrid(Vector3 center, float size, float countCells)
    {
#if UNITY_EDITOR
        Vector3 gridCenter = center + new Vector3(-size / 2f + 0.5f, 0f, -size / 2f + 0.5f);
        for (int i = 0; i < size; i++)
        {
            for (int j = 0; j < size; j++)
            {
                if (countCells <= i * size + j)
                    return;

                var cellCenter = PhysicsUtils.RaycastFromUpToDown(gridCenter + new Vector3(i, 0, j), Consts.LayerMasks.GroundForUnits).point;
                Gizmos.DrawCube(cellCenter, new Vector3(0.9f, 1f, 0.9f));
            }
        }
#endif
    }

    public static void DrawRect(Rect rect, float height, float startPosY)
    {
#if UNITY_EDITOR
        var p1 = new Vector2(rect.xMin, rect.yMin);
        var p2 = new Vector2(rect.xMin, rect.yMax);
        var p3 = new Vector2(rect.xMax, rect.yMax);

        float zHeight = p2.y - p1.y;
        float xWidth = p3.x - p2.x;
        Gizmos.DrawCube(new Vector3(p1.x, startPosY, p1.y + zHeight * 0.5f), new Vector3(0.1f, height, zHeight)); //left
        Gizmos.DrawCube(new Vector3(p1.x + xWidth * 0.5f, startPosY, p2.y), new Vector3(xWidth, height, 0.1f));//top

        Gizmos.DrawCube(new Vector3(p3.x, startPosY, p1.y + zHeight * 0.5f), new Vector3(0.1f, height, zHeight)); //right
        Gizmos.DrawCube(new Vector3(p1.x + xWidth * 0.5f, startPosY, p1.y), new Vector3(xWidth, height, 0.1f));//bottom
#endif
    }
}
