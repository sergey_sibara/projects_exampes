﻿using UnityEngine;
using System.Collections;

public class HideSelfInDesktop : MonoBehaviour
{
    private void Awake()
    {
#if UNITY_STANDALONE_WIN
        gameObject.SetActive(false);
#endif
    }
}
